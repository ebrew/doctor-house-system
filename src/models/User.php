<?php
class User{

    // Connection instance
    private $connection;

    // table name
    private $tableName = "users";

    // table columns
    public $id;
    public $firstName;
    public $lastName;
    public $userName;
    public $password;
    public $gender;
    public $type;
    public $email;
    public $phoneNumber;

    //constructor for class
    public function __construct($connection){
        $this->connection = $connection;
    }
    public function joke(){
        echo("hello");
    }
    //CRUD functions

    //Create a new user and insert their details into the database
    public function create(){

        //check if the said user already exists

        //write query
        $query = "INSERT INTO users( firstName, lastName, userName, password, gender, type,email , phoneNumber) VALUES(:firstName, :lastName, :userName, :password, :gender, :type, :email , :phoneNumber)";
        
        $stmt = $this->connection->prepare($query);
        
        //edit values before posting them
        $this->firstName=strip_tags($this->firstName);
        $this->lastName=strip_tags($this->lastName);
         $this->userName=strip_tags($this->userName);
        $this->password=password_hash(strip_tags($this->password),PASSWORD_DEFAULT);
        $this->gender=strip_tags($this->gender);
        $this->type=strip_tags($this->type);
        $this->email=strip_tags($this->email);
        $this->phoneNumber=strip_tags($this->phoneNumber);
        
 
        // bind values 
        $stmt->bindParam(":firstName", $this->firstName);
        $stmt->bindParam(":lastName", $this->lastName);
        $stmt->bindParam(":userName", $this->userName);
        $stmt->bindParam(":password", $this->password);
        $stmt->bindParam(":gender", $this->gender);
        $stmt->bindParam(":type", $this->type);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":phoneNumber", $this->phoneNumber);
        if($stmt->execute()){
            $this->id = $this->connection->lastInsertId();
            //echo("true");
            return true; //indicate if the user was created or not
            
        }else{
            //echo("false");
            return false;
            
        }
 
    }
    
    //Read functions

    //returns all the users in the database
    public function readAll(){
        $type = "type";
        $query = "SELECT
                id, firstName  lastName, userName,  gender, email, phoneNumber
            FROM
                " . $this->tableName ;

        $stmt = $this->connection->prepare($query);
    
        if($stmt->execute()){
        }

        $results = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $results;
    }
    public function readById(){
        $query = "SELECT
                id, firstName, lastName, userName, gender, type, email, phoneNumber
            FROM
                " . $this->tableName . "
            WHERE
                id = ?";

        $stmt = $this->connection->prepare( $query );
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    public function readByUserName(){
        $query = "SELECT
                id, firstName, lastName, userName, gender, type, email, phoneNumber
            FROM
                " . $this->tableName . "
            WHERE
                id = ?";

        $stmt = $this->connection->prepare( $query );
        $stmt->bindParam(1, $this->userName);
        $stmt->execute();
        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //Update
    public function update($id){
        $query = "UPDATE
                " . $this->tableName . "
            SET
                firstName = :firstName,
                lastName = :lastName,
                userName = :userName,
                password = :password,
                email = :email,
                phoneNumber = :phoneNumber,
                type = :type,
                gender = :gender
            WHERE
                id = :id";
 
    // prepare query statement
    $stmt = $this->connection->prepare($query);

    $this->firstName=htmlspecialchars(strip_tags($this->firstName));
    $this->lastName=htmlspecialchars(strip_tags($this->lastName));
    $this->password=password_hash(htmlspecialchars(strip_tags($this->password)),PASSWORD_DEFAULT);
    $this->gender=htmlspecialchars(strip_tags($this->gender));
    $this->type=htmlspecialchars(strip_tags($this->type));
    $this->email=htmlspecialchars(strip_tags($this->email));
    $this->phoneNumber=htmlspecialchars(strip_tags($this->phoneNumber));

    $stmt->bindParam(":id", $id);
    $stmt->bindParam(":firstName", $this->firstName);
    $stmt->bindParam(":lastName", $this->lastName);
    $stmt->bindParam(":userName", $this->userName);
    $stmt->bindParam(":password", $this->password);
    $stmt->bindParam(":gender", $this->gender);
    $stmt->bindParam(":type", $this->type);
    $stmt->bindParam(":email", $this->email);
    $stmt->bindParam(":phoneNumber", $this->phoneNumber);
      

        if($stmt->execute()){
            //echo("true");
            return true; //indicate if the user was created or not
            
        }else{
            //echo("false");
            return false;
            
        }
 
    }

    //Delete the object from the database
    public function delete(){
        // delete query
        $query = "DELETE  FROM users WHERE id = ?";
    
        // prepare query
        $stmt = $this->connection->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;

    }
}
?>