
<?php

class Appointment {
    // Model level variables
    public $id;
    public $patientId;
    public $doctorId;
    public $date;
    public $startTime;
    public $duration;
    public $patientRemarks ;
    public $doctorRating;
    public $doctorRemarks;
    public $modified;

    // this holds the connection to the database
    private $connection;

    // the name of the table in the database for this model
    private $tablename = "appointments";

    public function __construct($conn){
        $this->connection = $conn;
    }

    // this adds a new appointment into the table
    public function create(){
        //write query
        $query = "INSERT INTO appointments(patientId,doctorId,date,startTime, duration,patientRemarks,doctorRating,doctorRemarks) VALUES(:patientId,:doctorId,:date,:startTime, :duration,:patientRemarks,:doctorRating,:doctorRemarks)";
        $stmt  = $this->connection->prepare($query);
        // posted values
        $this->doctorRemarks=htmlspecialchars(strip_tags($this->doctorRemarks));
        $this->patientRemarks=htmlspecialchars(strip_tags($this->patientRemarks));


        // bind values
        $stmt->bindParam(":doctorRemarks", $this->doctorRemarks);
        $stmt->bindParam(":patientRemarks", $this->patientRemarks);
        $stmt->bindParam(":patientId", $this->patientId);
        $stmt->bindParam(":doctorId", $this->doctorId);
        $stmt->bindParam(":date", $this->date);
        $stmt->bindParam(":duration", $this->duration);
        $stmt->bindParam(":startTime", $this->startTime);        
        $stmt->bindParam(":doctorRating", $this->doctorRating);
        if($stmt->execute()){
            $this->id = $this->connection->lastInsertId();
            return true;
        }else{
            return false;
        }

    }


    public function update(){
        // update query
        $query = "UPDATE
                " . $this->tablename . "
            SET
                date = :date,
                startTime = :startTime,
                duration = :duration,
            WHERE
                id = :id";
        // prepare query statement
        $stmt = $this->connection->prepare($query);

        // sanitize input
        $this->date=strip_tags($this->date);
        $this->duration=strip_tags($this->duration);
        $this->startTime=strip_tags($this->startTime);


        $stmt->bindParam(":date", $this->date);
        $stmt->bindParam(":duration", $this->duration);
        $stmt->bindParam(":startTime", $this->startTime);
        $stmt->bindParam(":id", $this->id);
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    //Delete the object from the database
    public function delete($id){
        // delete query
        $query = "DELETE  FROM appointments WHERE id = ?";
    
        // prepare query
        $stmt = $this->connection->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;

    }

    // this returns all the appointments in the table
    public function readAll(){
        $query = "SELECT * FROM ". $this->tablename ." ORDER BY date ";
        $stmt  = $this->connection->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $results;
    }
    // this returns a single appointment by using the an id
    public function readById($id){
        $query = "SELECT * FROM " . $this->tablename . " WHERE id = ? limit 0,1";
        $stmt = $this->connection->prepare( $query );
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    // this returns all the doctor's appointments ordered by the date in descending order
    public function readByDoctor($doctorId){
        $query = "SELECT * FROM " . $this->tablename . " WHERE doctorId = ? ORDER BY date DESC";
        $stmt = $this->connection->prepare( $query );
        $stmt->bindParam(1, $doctorId);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }
    // this returns all the patient's appointments ordered by date in descending order
    public function readByPatient($patientId){
        $query = "SELECT * FROM " . $this->tablename . " WHERE patientId = ? ORDER BY date DESC";
        $stmt = $this->connection->prepare( $query );
        $stmt->bindParam(1, $patientId);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

    // this returns all the doctor-patient history for a appointments
    public function readByDoctorAndPatient($doctorId, $patientId){
        $query = "SELECT * FROM " . $this->tablename . " WHERE patientId = ? AND doctorId = ? ORDER BY date DESC";
        $stmt = $this->connection->prepare( $query );
        $stmt->bindParam(1, $patientId);
        $stmt->bindParam(2, $doctorId);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $results;
    }


}

?>