<?php


class Doctor {
    public $doctorId;
    public $specialty;
    public $experience;
    public $cases;
    public $rating;

    // this holds the connection to the database
    private $connection;

    // the name of the table in the database for this model
    private $tablename = "doctors";

    public function __construct($conn){
        $this->connection = $conn;
    }

    // this adds a new appointment into the table
    public function create(){
        //write query
        $query = "INSERT INTO doctors(doctorId,specialty,experience,cases,rating) VALUES(:doctorId,:specialty,:experience,:cases,:rating)";
        $stmt  = $this->connection->prepare($query);
        // posted values
        $this->specialty=strip_tags($this->specialty);
        $this->experience=strip_tags($this->experience);
        $this->cases=strip_tags($this->cases);
        

 
        // bind values
        $stmt->bindParam(":doctorId", $this->doctorId);
        $stmt->bindParam(":specialty", $this->specialty);
        $stmt->bindParam(":experience", $this->experience);
        $stmt->bindParam(":cases", $this->cases);
        $stmt->bindParam(":rating", $this->rating);
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }

    }
    // this returns a single doctor by using the an id
    public function readById($id){
        $query = "SELECT * FROM " . $this->tablename . " WHERE doctorId = ? limit 0,1";
        $stmt = $this->connection->prepare( $query );
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function update($id){
        // update query
        $query = "UPDATE
                " . $this->tablename . "
            SET
                specialty = :specialty,
                experience = :experience,
                cases = :cases,
                rating = :rating,
            WHERE
                doctorId = :id";
        // prepare query statement
        $stmt = $this->connection->prepare($query);

        // sanitize input
        $this->specialty=htmlspecialchars(strip_tags($this->specialty));
        $this->experience=htmlspecialchars(strip_tags($this->experience));
        $this->cases=htmlspecialchars(strip_tags($this->cases));
        $this->rating=htmlspecialchars(strip_tags($this->rating));


        $stmt->bindParam(":specialty", $this->specialty);
        $stmt->bindParam(":experience", $this->experience);
        $stmt->bindParam(":cases", $this->cases);
        $stmt->bindParam(":rating", $this->rating);
        $stmt->bindParam(":id", $id);
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    // this returns all the doctors in the table
    public function readAll(){
        $query = "SELECT * FROM ". $this->tablename ."";
        $stmt  = $this->connection->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $results;
    }

}








?>