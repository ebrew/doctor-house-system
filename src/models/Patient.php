<?php

class Patient{

    // Connection instance
    private $connection;

    // table name
    private $tableName = "patients";

    // table columns
    public $patientId;
    public $dateOfBirth;



    //constructor for class
    public function __construct($connection){
        $this->connection = $connection;
    }
 
    //CRUD functions

    //Create a new user and insert their details into the database
    public function create(){

        //check if the said user already exists

        //write query
        $query = "INSERT INTO patients( patientId, dateOfBirth) VALUES(:patientId, :dateOfBirth)";
        
        $stmt = $this->connection->prepare($query);
        
        //edit values before posting them
        //$this->firstName=htmlspecialchars(strip_tags($this->dateOfBirth));
        
        // bind values 
        $stmt->bindParam(":patientId", $this->patientId);
        $stmt->bindParam(":dateOfBirth", $this->dateOfBirth);

    
        if($stmt->execute()){
            return true; //indicate if the user was created or not
            
        }else{
            echo("false");
            return false;
            
        }
 
    }
    
    //Read functions

    //returns all the users in the database
    public function readAll(){
        $type = "type";
        $query = "SELECT
                patientId, dateOfBirth
            FROM
                " . $this->tableName ;

        $stmt = $this->connection->prepare($query);
    
        if($stmt->execute()){}

        $results = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $results;
    }
    public function readById(){
        $query = "SELECT
                patientId, dateOfBirth
            FROM
                " . $this->tableName . "
            WHERE
                id = ?";

        $stmt = $this->connection->prepare( $query );
        $stmt->bindParam(1, $this->patientId);
        $stmt->execute();
        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }


    //Update
    public function update($patientId){
        $query = "UPDATE patients
            SET
                dateOfBirth = :dateOfBirth
            WHERE
                patientId = :id";
 
    // prepare query statement
    $stmt = $this->connection->prepare($query);

    $this->dateOfBirth=htmlspecialchars(strip_tags($this->dateOfBirth));

    $stmt->bindParam(":id", $patientId);
    $stmt->bindParam(":dateOfBirth", $this->dateOfBirth);
      

        if($stmt->execute()){
            echo("true");
            return true; //indicate if the user was created or not
            
        }else{
            echo("false");
            return false;
            
        }
 
    }

    //Delete the object from the database
    public function delete($patientId){
        // delete query
        $query = "DELETE  FROM patients WHERE patientId = ?";
    
        // prepare query
        $stmt = $this->connection->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($patientId));
    
        // bind id of record to delete
        $stmt->bindParam(1, $patientId);
    
        // execute query
        if($stmt->execute()){
            echo("true");
            return true;
        }
    
        return false;

    }
}
?>