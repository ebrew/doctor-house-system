
/*Create the table for the users, both patients and doctors*/
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(256) NOT NULL,
  `lastName` varchar(256) NOT NULL,
  `userName` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `gender` char(1) NOT NULL, /*M represents male, F represents female and N represents prefer not to say*/
  `type`  char(1) NOT NULL, /*D represents doctor and P reperesents patient*/
  `email` varchar(256) NOT NULL, 
  `phoneNumber` int(15) NOT NULL, 
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;


/*Specific table only for patients and  patient details*/
CREATE TABLE IF NOT EXISTS `patients` (
  `patientId` int(11) NOT NULL,
  `dateOfBirth` date NOT NULL,

  CONSTRAINT fk_user_patient_id
  FOREIGN KEY (`patientId`)
  REFERENCES users (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;


/*Specific table only for doctors and doctor details*/
CREATE TABLE IF NOT EXISTS `doctors` (
  `doctorId` int(11) NOT NULL,
  `specialty` varchar(256) NOT NULL,
  `experience` int(11) NOT NULL, /*How many year of experience the doctor has*/
  `cases` int(12) NOT NULL, /*total number of cases the doctor has handled*/
  `rating` decimal, /*the doctors rating, 0 - 5*/
  CONSTRAINT fk_user_doctor_id
  FOREIGN KEY (`doctorId`)
  REFERENCES users (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

/*Apointment table holds the details of an appointment between a doctor and a patient*/
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patientId`int(11) NOT NULL,
  `doctorId` int(11) NOT NULL,
  `date` date NOT NULL,
  `startTime` time NOT NULL,
  `duration` int(11) NOT NULL, /*duration in seconds*/
  `patientRemarks` text,
  `doctorRating` decimal, /*0 to 5 indicates the patients rating of the experience*/
  `doctorRemarks` text,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;
