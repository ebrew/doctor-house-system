<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include '../../../config/Database.php';
include '../../models/User.php';

$db = new Database();
$conn = $db->connect();

$user = new User($conn);

// get posted data
$data = json_decode(file_get_contents("php://input"));
// make sure data is not empty
if(
    !empty($data->firstName) &&
    !empty($data->lastName) &&
    !empty($data->userName) &&
    !empty($data->password) &&
    !empty($data->gender) &&
    !empty($data->type) &&
    !empty($data->email) &&
    !empty($data->phoneNumber)
){
 
    // set user property values
    $user->phoneNumber = $data->phoneNumber;
    $user->email =  $data->email;
    $user->firstName = $data->firstName;
    $user->lastName = $data->lastName;
    $user->userName = $data->userName;
    $user->password = $data->password;
    $user->gender = $data->gender;
    $user->type = $data->type;

    // create the product
    if($user->create()){

        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode($user);
    }
 
    // if unable to create the product, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create user."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create user. Data is incomplete."));
}
?>