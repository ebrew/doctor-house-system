<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include '../../../config/Database.php';
include '../../models/Appointment.php';

$db = new Database();
$conn = $db->connect();

$appointment = new Appointment($conn);

// get posted data
$data = json_decode(file_get_contents("php://input"));
// make sure data is not empty
if(
    !empty($data->date) &&
    !empty($data->startTime) &&
    !empty($data->id) &&
    !empty($data->duration)
){
 
    // set appointment property values
    $appointment->date = DateTime::updateFromFormat('Y/m/d', $data->date)->format('Y/m/d');
    $appointment->startTime = DateTime::updateFromFormat('H:i:s', $data->startTime)->format('H:i:s');
    $appointment->duration = $data->duration;
    $appointment->id = $data->id;

    // update the product
    if($appointment->update()){

        // set response code - 201 updated
        http_response_code(201);
 
        // tell the user
        echo json_encode($appointment);
    }
 
    // if unable to update the product, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to update appointment."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to update appointment. Data is incomplete."));
}
?>