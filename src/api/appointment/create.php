<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include '../../../config/Database.php';
include '../../models/Appointment.php';

$db = new Database();
$conn = $db->connect();

$appointment = new Appointment($conn);

// get posted data
$data = json_decode(file_get_contents("php://input"));
// make sure data is not empty
if(
    !empty($data->date) &&
    !empty($data->startTime) &&
    !empty($data->patientId) &&
    !empty($data->doctorId) &&
    !empty($data->duration)
){
 
    // set appointment property values
    $appointment->date = DateTime::createFromFormat('Y/m/d', $data->date)->format('Y/m/d');
    $appointment->startTime = DateTime::createFromFormat('H:i:s', $data->startTime)->format('H:i:s');
    $appointment->patientId = $data->patientId;
    $appointment->doctorId = $data->doctorId;
    $appointment->duration = $data->duration;

    // create the product
    if($appointment->create()){

        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode($appointment);
    }
 
    // if unable to create the product, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create appointment."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create appointment. Data is incomplete."));
}
?>