<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include '../../../config/Database.php';
include '../../models/Doctor.php';

$db = new Database();
$conn = $db->connect();

$doctor = new Doctor($conn);

if (!isset($_GET['id'])){
    die();
}else{
    if ($doctor->delete($_GET['id'])){
        http_response_code(200);
        echo json_encode(array("message" => "doctor has been successfully deleted"));
    }else{
        // set response code - 404 Not found
        http_response_code(404);
        // tell the user product does not exist
        echo json_encode(array("message" => "could not delete the doctor"));
    }
}



?>