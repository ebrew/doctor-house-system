<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include '../../../config/Database.php';
include '../../models/Doctor.php';

$db = new Database();
$conn = $db->connect();

$doctor = new Doctor($conn);

// get posted data
$data = json_decode(file_get_contents("php://input"));
// make sure data is not empty
if(
    !empty($data->doctorId) &&
    !empty($data->experience) &&
    !empty($data->cases) &&
    !empty($data->specialty)
){
 
    // set doctor property values
    $doctor->doctorId = $data->doctorId;
    $doctor->experience = $data->experience;
    $doctor->cases = $data->cases;
    $doctor->specialty = $data->specialty;

    // create the product
    if($doctor->create()){

        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode($doctor);
    }
 
    // if unable to create the product, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create doctor."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create doctor. Data is incomplete."));
}
?>