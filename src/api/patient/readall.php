<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include '../../../config/Database.php';
include '../../models/Patient.php';

$db = new Database();
$conn = $db->connect();

$patient = new Patient($conn);
$apps =  $patient->readAll();
if ($apps!=null){
    http_response_code(200);
    // make it json format
    echo json_encode($apps);
}else{
    // set response code - 404 Not found
     http_response_code(404);
    // tell the user product does not exist
    echo json_encode(array("message" => "No patients found"));
}





?>