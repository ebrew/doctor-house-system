<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include '../../../config/Database.php';
include '../../models/Patient.php';

$db = new Database();
$conn = $db->connect();

$patient = new Patient($conn);

// get posted data
$data = json_decode(file_get_contents("php://input"));
// make sure data is not empty
if(
    !empty($data->patientId) &&
    !empty($data->dateOfBirth)

){
 
    // set patient property values
    $patient->dateOfBirth = DateTime::createFromFormat('Y/m/d', $data->dateOfBirth)->format('Y/m/d');
    $patient->patientId = $data->patientId;

    // create the product
    if($patient->create()){

        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode($patient);
    }
 
    // if unable to create the product, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create patient."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create patient. Data is incomplete."));
}
?>