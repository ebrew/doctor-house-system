<?php

//specifies configuration settings for connecting to database
class Database{
 
    // database credentials
    private $host = "localhost";
    private $db_name = "doctorHaus";
    private $username = "root";
    private $password = "password";
    public $conn;
 
    // connect to database or handle error if there is an exception
    public function connect(){
 
        $this->conn = null;
 
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}
?>