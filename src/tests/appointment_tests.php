<?php

include '../../config/Database.php';
include '../models/Appointment.php';

date_default_timezone_set('UTC');

$db = new Database();
$conn = $db->connect();

$appointment = new Appointment($conn);
$appointment->patientId = 19;
$appointment->doctorId = 20;
$appointment->date =  date("Y/m/d");
$appointment->startTime = date("h:i:sa");
$appointment->duration = 6;
$appointment->patientRemarks = "Nice, good " ;
$appointment->doctorRating = 4;
$appointment->doctorRemarks = "Played cool during the session";
// $appointment->create();

$res = $appointment->readAll();
foreach ($res as $ap) {
    echo($ap->patientRemarks);
    echo "<br>";
}
$appReadById = $appointment->readById(20);
echo($appReadById['doctorRemarks']);


$appointment->date = date("Y/m/d");
$appointment->startTime = date("h:i:sa");
$appointment->duration = 9;
$appointment->update(20);
?>