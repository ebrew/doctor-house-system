import React from 'react';
import Home from './Home/Home'
import './App.css';
import Header from './Layout/Header/Header';
import Footer from './Layout/Footer/Footer'
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AboutUs from './Pages/AboutUs/AboutUs'
import Services from './Pages/Services/Services'
import Contact from './Pages/Contact/Contact'
import SignUp from './Pages/SignUp/SignUp'
import SignIn from './Pages/SignIn/SignIn'
import Dashboard from './Dashboard/Dashboard';
import Patients from './Pages/Dashboard/Patients/Patients'

function App() {
  return (
    <div className="App">
     
      <BrowserRouter>
          <div className="Auth">
            <Switch>
            <Route exact path="/" component={Home} >
              <Header/>
              <Home/>
              <Footer/>
              </Route> 
              <Route exact path="/about" component={AboutUs} >
              <Header/>
              <AboutUs/>
              <Footer/>
              </Route> 
              <Route exact path="/contact" component={Contact} >
              <Header/>
              <Contact/>
              <Footer/>
              </Route> 
              <Route exact path="/services" component={Services} >
              <Header/>
              <Services/>
              <Footer/>
              </Route> 
              
              <Route path="/signup" component={SignUp} />
              <Route path="/signin" component={SignIn} />
              <Route path="/dashboard" component={Dashboard}/>
              <Route exact path="/patients" component={Dashboard} >
              <Dashboard/>
              <Patients/>
              </Route> 
            </Switch>
          </div>
        </BrowserRouter>
    </div>
  );
}

export default App;
