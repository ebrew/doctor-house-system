import React, { Component } from 'react'
import Footer from '../Layout/Footer/Footer'
import Typography from '@material-ui/core/Typography';
import Carousel from '../Layout/Component/Carousel'
import './Home.css'



class Home extends Component {
    render() {
        return (
            <div>
              <Carousel/>
                <div className="Container">
                
                    <div> 
                    <h2 className="center blue-text">Welcome</h2>
                        <div class="row a">
                          
                        <div class="col s12 m5 center">
                        
                      <Typography variant="h5">
                         St Luke Cardiac Clinic is a specialized clinic focusing on Heart (Cardiac) related diseases.
                          Presently, most of the existing hospitals and clinics in Ghana are focused on general healthcare service delivery. 
                          St Luke Cardiac Clinic however deals with heart illnesses and its related complications.
          </Typography> 
          </div>
           <div class="col s16 center">

                        <img src={require('../resources/images/welcomeface.jpg')} height="400" width="500" alt=""></img >  
                        </div>
          </div>

          
          <div className="down a ">
          <h2 className="center  red-text">Why Choose Us</h2>
             <div className="row b">
                 <div className="col server a center">
                 <img src={require('../resources/images/thumbs.png')} height="150" width="150" alt=""></img >
                 <p className="blue-text">Quality Service</p>
             </div>
             <div className="col server a center">
             <img src={require('../resources/images/globe.png')} height="150" width="150" alt=""></img >
              <p className="blue-text">Internation Standards</p>
             </div>
             <div className="col server a center ">
             <img src={require('../resources/images/calend.png')} height="150" width="150" alt=""></img >
             <p className="blue-text">Flexible Schedule</p>
             </div>
           </div>
           </div>
           
   <div className=" Wrapper h center">
   <h2 className="brid blue-text">Our_Services</h2>
   <img src={require('../resources/images/cardiac.jpg') } alt="" width="500px" height="400px" ></img >
   <img src={require('../resources/images/echo4.jpg') } alt="" width="500px" height="400px" ></img >
   <img src={require('../resources/images/holter.jpg') } alt="" width="500px" height="400px"></img >
   </div>
   </div>
  </div>
                </div>
        
        )
    }
}

export default Home;