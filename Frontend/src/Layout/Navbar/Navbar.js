import React from "react";
import './Navbar.css'

import { BrowserRouter } from "react-router-dom";

const Navbar = () => {
  return (
    
      <div >
        <nav className="z-depth-1 blue">
            
            <ul className="">
              <BrowserRouter>
                <router>
                  <li>
                    <a href="/">Home</a>
                  </li>
                  <li>
                    <a href="/services">Services</a>
                  </li>
                  <li>
                    <a href="/about">About Us</a>
                  </li>
                  <li>
                    <a href="/contact">Contact Us</a>
                  </li>
                </router>
              </BrowserRouter>
            </ul>
          
        </nav>
      
</div>
     
    
  );
};

export default Navbar;