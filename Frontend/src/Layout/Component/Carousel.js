import React, { Component } from 'react';
import Slider from 'infinite-react-carousel';
import './Carousel.css'
import { sizeWidth } from '@material-ui/system';

class Carousel extends Component {
  render() {
    const settings =  {
      arrowsBlock: false,   
      autoplay: true,
      sizeWidth: 100
     
    };
    return (
      <div className="center">
        
        <Slider { ...settings }>
          <div >
          <img src={require('../../resources/images/banner3.jpg') } alt="" width="100%" height="100%"></img >
          </div>
          <div>
          <img src={require('../../resources/images/lukebanner.jpg') } alt="" width="100%" height="100%"></img >
          </div>
          <div>
          <img  src={require('../../resources/images/lukebanner3.jpg') } alt="" width="100%" height="100%"></img >
          </div>
        </Slider>
      </div>
    );
  }
}
export default Carousel;