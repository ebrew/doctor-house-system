import React, { Component } from 'react'
import Navbar from '../Navbar/Navbar'
import AppBar from '../AppBar/AppBar'
import './Header.css'


class Header extends Component {
    render() {
        return (
            <div>
          <AppBar/>
            <div class="Container">

            
            <div class="row">

              <div class="col l2 s2">  
                <p className="image"><a href="/"><img src={require('../../resources/images/newlogo.v1.jpg') } alt=""></img ></a></p>
              </div>

              <div class="col 15 s5">
              <h1 class="blue-text center ">St. Luke's</h1>
              </div>
              <div class="col 17 s12">
                <h1 class="red-text center">Cardiac Clinic</h1>
                
              </div>
              
             
            </div>
          
          </div>
         
            
         <header>
         <Navbar/>
    </header>
    </div>
        )
    }
}
export default Header;