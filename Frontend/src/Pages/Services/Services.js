import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography';
import './Services.css'




class Services extends Component {
    render() {
        return (
            <div className= "whole">
            <div className= "Wrapper center white-text">
                <h1 className="service">Our Services</h1>
            </div>
            <div className= "Wrapper blue">
            <p>
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </p>
             <p>
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </p>
             <p>
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </p>
            </div>

            <div className="row">
             <div className="services l">
             <img src={require("../../resources/images/screening.jpg")} />
             
           </div>
           <div className="col services k">
           <h4>Cardiac Consultations/ Screening</h4>
             <Typography variant="h6">
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </Typography>
           </div>  
           <div className="services l">
             <img src={require("../../resources/images/consult.jpg")} />
           </div>
           <div className="col services k">
           <h4>Second Opinion Consultation</h4>
             <Typography variant="h6">
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </Typography>
           </div>  
           </div>
           <div className="row">
             <div className="services l">
             <img src={require("../../resources/images/preg.jpg")} />
            
           </div>
           <div className="col services k">
           <h4>Cardiac Problems during Pregnancy</h4>
             <Typography variant="h6">
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </Typography>
           </div>  
           <div className="services l">
             <img src={require("../../resources/images/echocheck.png")} />
           </div>
           <div className="col services k">
           <h4>Echo</h4>
             <Typography variant="h6">
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </Typography>
           </div>  
           </div>
           <div className="row">
             <div className="services l">
             <img src={require("../../resources/images/ambp.jpg")} />
           </div>
           <div className="col services k">
           <h4>24hour AMBP Monitoring (ABPM)</h4>
             <Typography variant="h6">
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </Typography>
           </div>  
           <div className="services l">
             <img src={require("../../resources/images/ecg.jpg")} />
           </div>
           <div className="col services k">
           <h4>ECG</h4>
             <Typography variant="h6">
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </Typography>
           </div>  
           </div>
           <div className="row">
             <div className="services l">
             <img src={require("../../resources/images/holtermon.jpg")} />
           </div>
           <div className="col services k">
           <h4>Holter Monitoring</h4>
             <Typography variant="h6">
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </Typography>
           </div>  
           <div className="services l">
             <img src={require("../../resources/images/PAD.jpg")} />
           </div>
           <div className="col services k">
           <h4>PAD Screening</h4>
             <Typography variant="h6">
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </Typography>
           </div>  
           </div>
           <div className="down r">
          
             <div className="pic">
           <img src={require("../../resources/images/steth.jpg")} height="400px"/>
           </div>
           <h2 className=" blue-text">How can we Help you</h2>
           </div>

           </div>
        )
    }
}

export default Services;