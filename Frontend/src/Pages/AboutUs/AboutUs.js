import React, { Component } from 'react'
import './AboutUs.css'

class AboutUs extends Component {
    render() {
        return (
            <div>
            <div className= "wrapper b">
                </div>
                <div className="row">
             <div className="col serve center" >
             <h4 className="bold blue-text">Who We Are</h4>
             <p>
             St Luke Cardiac Clinic is a specialized clinic for treating Heart related diseases. 
             Due to lifestyle changes, heart disease is on the increase in Ghana and like many developed countries, 
             it is becoming a leading cause of death in Africa. Young people are dying from heart diseases, 
             many of them have sudden cardiac arrest. 
             Due to the complexity of Heart problems well trained personnel are required for making correct diagnosis and prescribing optimal treatment.
              Today, many Ghanaians still travel abroad for medical consultations due to the lack of adequately equipped facilities. 
              St Luke's Cardiac Clinic provides high quality healthcare services at affordable prices to all Ghanaians and clients from the sub regions. 
              The services include Heart Screening and Consultation, ECG with report, Echocardiography (ultrasound / scan of the heart), 
              24 hour Ambulatory BP monitoring, Holter monitoring, Peripheral Artery disease Doppler with ABI test, Heart problems in Pregnancy,
               Pharmacy/ Dispensary and Second opinion consultation.
             </p>
           </div>
           <div className="col serve">
             <img src={require("../../resources/images/room.jpg")} />
           </div>
            </div>
            
            
           
             <div className="down b center">
             <h2 className="center blue-text">Echo Department</h2>
             <img src={require("../../resources/images/echodep.jpg")} height="380"/>
             <img src={require("../../resources/images/echodep2.jpg")} height="380"/>
             </div>
        
             <div className="down c center">
             <h2 className="center blue-text">ECG Department</h2>
             <img src={require("../../resources/images/ecgdep.jpg")} height="380"/>
             <img src={require("../../resources/images/ecgdep2.jpg")} height="380"/>
             </div>
             <div className="down d center">
             <h2 className="center blue-text">Consultation Department</h2>
             <img src={require("../../resources/images/consultdep.jpg")} height="380"/>
             <img src={require("../../resources/images/consultdep2.jpg")} height="380"/>
             </div>
             <div className="down e center">
             <h2 className="center blue-text">Research Department</h2>
             <img src={require("../../resources/images/researchdep.jpg")} height="380" width="800"/>
             <img src={require("../../resources/images/researchdep2.jpg")} height="380" width="800"/>
             </div>

             <div className="down d center white">
             <div className="row center">
                 <div className="col services a centre">
             <p>
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </p>
             </div>
             <div className="col services a center">
             <p>
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </p>
             </div>
             <div className="col services a center">
             <p>
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
               loremAute ad ullamco dolore laboris fugiat et tempor magna ex incididunt
               esse.
             </p>
             </div>
           </div>
           </div>
             
            </div>
        )
    }
}       

export default AboutUs;
