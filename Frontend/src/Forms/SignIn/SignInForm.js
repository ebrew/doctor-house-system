import React, { Component } from 'react'
import './SignInForm.css'

class SignInForm extends Component {
    render() {
        return (
            <div>
        
        <div id="signin-page" class="row">
          <div class="col s12 z-depth-6 card-panel">
            <form class="signin-form">
              <div class="row margin">
                <div class="input-field col s12">
                  <i class="mdi-social-person-outline prefix" />
                  <input id="user_name" type="text" class="validate" />
                  <label for="user_name" class="center-align">
                    Username/Email
                  </label>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12">
                  <i class="mdi-action-lock-outline prefix" />
                  <input id="user_passw" type="password" class="validate" />
                  <label for="user_passw">Password</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <a
                    href="register.html"
                    class="btn blue waves-effect waves-light col s12"
                  >
                    LOG IN
                  </a>
                </div>
                <div class="input-field col s12">
                  <p class="margin center medium-small sign-up">
                    Don't have an account? <a href="/dasboard">Sign Up</a>
                  </p>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}


export default SignInForm
