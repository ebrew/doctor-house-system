\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {subsection}{\numberline {1.1}Background to course}{3}
\contentsline {subsection}{\numberline {1.2}Background to project}{3}
\contentsline {subsection}{\numberline {1.3}Team work and member contributions}{4}
\contentsline {section}{\numberline {2}Software Design and Specifications}{5}
\contentsline {subsection}{\numberline {2.1}Entity Modeling}{5}
\contentsline {subsection}{\numberline {2.2}Entity Relations}{6}
\contentsline {subsection}{\numberline {2.3}Software Use Cases}{6}
\contentsline {subsection}{\numberline {2.4}Conclusion}{7}
